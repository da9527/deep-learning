# deep-learning

#### 介绍
[动手学深度学习](https://zh-v2.d2l.ai/index.html)

> \\ 换行
$z = x + y \\ 
c = a * b \\
z = x + y$

> \quad 空格
$a \ b$ \
$a \quad b$ \
$a \qquad b$

> _ 下标
$a_n$
​

> ^ 上标
$a^n$
 

> {} 一组内容
$a_{11} = b^{\frac{1}{2}}$

> \cdot 点乘
$z = x \cdot y$

> \times叉乘
$z = x \times y$

> \div 除以
$z = x \div y$

> \sqrt 根号 算术平方根
$\sqrt x$

> 其他
$\sqrt [n]x$
​
> \vec 矢量 \
$ \vec{ab} \ \overrightarrow{bc}$

> \prod连乘
$\prod_a^b$

> 角标在上边和下边的连乘
$\prod \limits_{i = 1}^n$
​

> \sum 连加
$\sum _a^b$
​
 
> 角标在上边和下边的连加
$\sum \limits _{i = 1}^n$
​

> \int 积分
$\int _a^b$
​

> 正负无穷积分
$\int _{-\infty}^{+\infty}$
​

> \partial 偏导
$\partial x$

> \propto 正比于
$a \propto b$

> \overline 上划线
$\overline {A \cdot B + B \cdot C}$
​

> \underline 下划线
$\underline {A \cdot B + B \cdot C}$

> \boxed 边框
$\boxed {x*y=z}$ \
$\fbox {x*y=z}$
​

> \mathbf 加粗
$\boxed{\mathbf {x*y=z}}$

> \boldsymbol 倾斜加粗
$\boxed{\boldsymbol {x*y=z}}$
​

> 比较运算符 \
\geq 大于等于 $a \geq b$ \
\leq 小于等于 $a \leq b$
\neq 不等于 $a \neq b$